package br.sceweb.teste;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import br.sceweb.modelo.Convenio;
import br.sceweb.modelo.ConvenioDAO;

public class UC05CadastrarConvenio {
	
	static ConvenioDAO convenioDAO;
	static Convenio convenio;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		convenio = new Convenio("23588764000117", "01/01/2016", "31/07/2016");
		convenioDAO = new ConvenioDAO();
	}
	
	@Test
	public void CT01UC05FB_convenio_sucesso() {
		assertEquals(1,ConvenioDAO.adiciona(convenio));
	}
	
	@Test
	public void CT02UC05A1_convenio_cadastrado() {
		assertEquals(0,ConvenioDAO.adiciona(convenio));
	}
	
	@Test
	public void CT03UC05A2_cnpj_invalido() {
		
	}
	
	@Test
	public void CT04UC05A3_cnpj_nao_cadastrado() {
		
	}
	
	@Test
	public void CT05UC05A4_dtIncio_maior() {
		
	}
	
	@Test
	public void CT06UC05A5_dtIncio_menor() {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
}